<?php

namespace App\Interfaces;

interface ImportClass {
    public function readFile($file);
}
