<?php

namespace App\Interfaces;

interface IConnection {
    public function connect();
}
