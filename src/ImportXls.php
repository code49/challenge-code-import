<?php

namespace App;

use App\Interfaces\ImportClass;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use App\Interfaces\IConnection;

use App\Database\Connection;
use App\Database\ConnectionXls;

class ImportXls implements ImportClass {

    private $connection = NULL;

    private function getConnection(){
        if(!$this->connection){
            $connection = Connection::getConnection(new ConnectionXls);
            $this->connection = $connection;
            return $connection;
        }

        return $this->connection;
    }

    public function readFile($file){


        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($file);
        
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = [];
        foreach ($worksheet->getRowIterator() AS $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $cell) {
                $cells[] = $cell->getValue();
            }
            $rows[] = $cells;
        }

        print_r($rows);
    }

}