<?php

namespace App;

use App\Interfaces\ImportClass;
use App\Interfaces\IConnection;

use App\Database\Connection;
use App\Database\ConnectionXml;

class ImportXml implements ImportClass {

    private $connection = NULL;

    private function getConnection(){
        if(!$this->connection){
            $connection = Connection::getConnection(new ConnectionXml);
            $this->connection = $connection;
            return $connection;
        }

        return $this->connection;
    }

    public function readFile($file){
    }

}