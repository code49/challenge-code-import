<?php

namespace App\Database;

use App\Interfaces\IConnection;


class Connection {

    public static function getConnection(IConnection $databaseConnection) {
        $connection = $databaseConnection->connect();
        return $connection;
}

}