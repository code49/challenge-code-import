<?php

namespace App\Database;

use App\Interfaces\IConnection;
use App\Database\Connection;

class ConnectionXls extends Connection implements IConnection {
    
    public function connect() {
        $connection = @new \mysqli('challenge-code-db', 'root', 'secret', 'import_xls');
		if(mysqli_connect_error())
			throw new \Exception("Erro ao se conectar com o banco de dados", 502);

        return $connection;

    }

}