<?php

namespace App;


use App\Interfaces\ImportClass;

class Import {

    public function __construct(ImportClass $importClass, $fileName){
        $importClass->readFile($fileName);
    }

    
}