-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: challenge-code-db:3306
-- Tempo de geração: 03-Nov-2021 às 19:03
-- Versão do servidor: 10.6.4-MariaDB-1:10.6.4+maria~focal
-- versão do PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `import_xml`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bairro`
--

CREATE TABLE `bairro` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cidade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `bairro`
--

INSERT INTO `bairro` (`id`, `nome`, `cidade`) VALUES
(3, 'Higienópolis', 1),
(4, 'Pinheiros', 1),
(5, 'Centro', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`id`, `nome`) VALUES
(1, 'São Paulo'),
(2, 'Bauru'),
(3, 'Pirajuí');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo_contato` tinyint(1) NOT NULL,
  `cidade` int(11) NOT NULL,
  `bairro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `endereco` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `email_contato`
--

CREATE TABLE `email_contato` (
  `id_contato` int(11) NOT NULL,
  `id_email` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imovel`
--

CREATE TABLE `imovel` (
  `id` int(11) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `transacao` tinyint(1) NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `descricao` text NOT NULL,
  `proprietario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone`
--

CREATE TABLE `telefone` (
  `id` int(11) NOT NULL,
  `numero` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone_contato`
--

CREATE TABLE `telefone_contato` (
  `id_contato` int(11) NOT NULL,
  `id_telefone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` (`id`, `descricao`) VALUES
(1, 'Casa'),
(2, 'Apartamento'),
(3, 'Sobrado'),
(4, 'Terreno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoContato`
--

CREATE TABLE `tipoContato` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `tipoContato`
--

INSERT INTO `tipoContato` (`id`, `descricao`) VALUES
(1, 'Proprietário'),
(2, 'Interessado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoContato_contato`
--

CREATE TABLE `tipoContato_contato` (
  `id_contato` int(11) NOT NULL,
  `id_tipoContato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_imovel`
--

CREATE TABLE `tipo_imovel` (
  `id_imovel` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacao`
--

CREATE TABLE `transacao` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `transacao`
--

INSERT INTO `transacao` (`id`, `descricao`) VALUES
(1, 'Venda'),
(2, 'Locação'),
(3, 'Lançamento'),
(4, 'Temporada');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacao_imovel`
--

CREATE TABLE `transacao_imovel` (
  `id_imovel` int(11) NOT NULL,
  `id_transacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `bairro`
--
ALTER TABLE `bairro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cidade` (`cidade`);

--
-- Índices para tabela `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bairro` (`bairro`),
  ADD KEY `cidade` (`cidade`);

--
-- Índices para tabela `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `email_contato`
--
ALTER TABLE `email_contato`
  ADD PRIMARY KEY (`id_contato`),
  ADD KEY `id_email` (`id_email`);

--
-- Índices para tabela `imovel`
--
ALTER TABLE `imovel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imovel_ibfk_1` (`proprietario`);

--
-- Índices para tabela `telefone`
--
ALTER TABLE `telefone`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `telefone_contato`
--
ALTER TABLE `telefone_contato`
  ADD PRIMARY KEY (`id_contato`),
  ADD KEY `id_telefone` (`id_telefone`);

--
-- Índices para tabela `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tipoContato`
--
ALTER TABLE `tipoContato`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tipoContato_contato`
--
ALTER TABLE `tipoContato_contato`
  ADD PRIMARY KEY (`id_contato`,`id_tipoContato`),
  ADD KEY `tipoContato_contato_ibfk_2` (`id_tipoContato`);

--
-- Índices para tabela `tipo_imovel`
--
ALTER TABLE `tipo_imovel`
  ADD PRIMARY KEY (`id_imovel`,`id_tipo`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- Índices para tabela `transacao`
--
ALTER TABLE `transacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `transacao_imovel`
--
ALTER TABLE `transacao_imovel`
  ADD PRIMARY KEY (`id_imovel`,`id_transacao`),
  ADD KEY `id_transacao` (`id_transacao`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `bairro`
--
ALTER TABLE `bairro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `cidade`
--
ALTER TABLE `cidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `imovel`
--
ALTER TABLE `imovel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `telefone`
--
ALTER TABLE `telefone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `tipoContato`
--
ALTER TABLE `tipoContato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `transacao`
--
ALTER TABLE `transacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `bairro`
--
ALTER TABLE `bairro`
  ADD CONSTRAINT `bairro_ibfk_1` FOREIGN KEY (`cidade`) REFERENCES `cidade` (`id`);

--
-- Limitadores para a tabela `contato`
--
ALTER TABLE `contato`
  ADD CONSTRAINT `contato_ibfk_1` FOREIGN KEY (`bairro`) REFERENCES `bairro` (`id`),
  ADD CONSTRAINT `contato_ibfk_2` FOREIGN KEY (`cidade`) REFERENCES `cidade` (`id`);

--
-- Limitadores para a tabela `email_contato`
--
ALTER TABLE `email_contato`
  ADD CONSTRAINT `email_contato_ibfk_1` FOREIGN KEY (`id_contato`) REFERENCES `contato` (`id`),
  ADD CONSTRAINT `email_contato_ibfk_2` FOREIGN KEY (`id_email`) REFERENCES `email` (`id`);

--
-- Limitadores para a tabela `imovel`
--
ALTER TABLE `imovel`
  ADD CONSTRAINT `imovel_ibfk_1` FOREIGN KEY (`proprietario`) REFERENCES `contato` (`id`);

--
-- Limitadores para a tabela `telefone_contato`
--
ALTER TABLE `telefone_contato`
  ADD CONSTRAINT `telefone_contato_ibfk_1` FOREIGN KEY (`id_contato`) REFERENCES `contato` (`id`),
  ADD CONSTRAINT `telefone_contato_ibfk_2` FOREIGN KEY (`id_telefone`) REFERENCES `telefone` (`id`);

--
-- Limitadores para a tabela `tipoContato_contato`
--
ALTER TABLE `tipoContato_contato`
  ADD CONSTRAINT `tipoContato_contato_ibfk_1` FOREIGN KEY (`id_contato`) REFERENCES `contato` (`id`),
  ADD CONSTRAINT `tipoContato_contato_ibfk_2` FOREIGN KEY (`id_tipoContato`) REFERENCES `tipoContato` (`id`);

--
-- Limitadores para a tabela `tipo_imovel`
--
ALTER TABLE `tipo_imovel`
  ADD CONSTRAINT `tipo_imovel_ibfk_1` FOREIGN KEY (`id_imovel`) REFERENCES `imovel` (`id`),
  ADD CONSTRAINT `tipo_imovel_ibfk_2` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id`);

--
-- Limitadores para a tabela `transacao_imovel`
--
ALTER TABLE `transacao_imovel`
  ADD CONSTRAINT `transacao_imovel_ibfk_1` FOREIGN KEY (`id_imovel`) REFERENCES `imovel` (`id`),
  ADD CONSTRAINT `transacao_imovel_ibfk_2` FOREIGN KEY (`id_transacao`) REFERENCES `transacao` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
