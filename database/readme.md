## Instruções

- Acesse o cpanel, de acordo com seu ambiente:
    - [Docker](http://localhost:8080)
    - [XAMPP](http://localhost/phpmyadmin)

- Importe os sql em cada banco de dados
- Lembre-se de atualizar os arquivos de conexão de cada banco de dados em `src/database` de acordo com seu ambiente.

## Credenciais Docker

Usuário | Senha 
--- | --- 
root | secret 