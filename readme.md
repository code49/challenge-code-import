# Challenge Import CODE 49


## Objetivo

o Objetivo desse teste é avaliar se o candidato(a) está preparado(a) para lidar com manipulação de dados de diferentes fontes e tipos.

- [ ] Importar os arquivos XLS para o banco `import_xls`
- [ ] Importar os arquivos XML para o banco `import_xml`

## Orientações

- Na pasta `public/data` você irá encontrar os arquivos que devem ser importados para os banco de dados.
- Importar os arquivos de `public/data/xls` para o banco `import_xls`.
- Importar os arquivos de `public/data/xml` para o banco `import_xml`.
- Utilizar as classes já pré definidas para cada implementação.

## Observações

Algumas observações:

- Certifique-se de manter os arquivos de conexão com o Banco organizados e estruturados para facilitar o teste em outros ambientes;
- Para o desenvolvimento do projeto é recomendado utilizar o [**Docker**](https://www.docker.com/), ou o [**XAMPP**](https://www.apachefriends.org/pt_br/index.html). Caso queira utilizar alguma outra ferramenta, o uso da mesma deverá ser documentado.
- Caso opte pelo uso do [**Docker**](https://www.docker.com/), já foi preparado um docker-compose file com as configurações do ambiente. (Não é obrigatório utiliza-lo)

## Ambiente Docker

- Abra o terminal e execute o comando `docker-compose build`.
- Após a finalização, execute o comando `docker-compose up -d` para subir os containers.
- Por fim, execute `docker exec -it challenge-code-apache composer install` e em seguida seleciona a opção **Y** para instalar as dependências do projeto.

## Ambiente XAMPP

- Instale em sua máquina o [**Composer**](https://getcomposer.org/download/).
- Abra o terminal e execute o comando `composer install`.